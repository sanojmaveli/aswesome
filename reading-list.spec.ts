import { $,$$, browser, ExpectedConditions,protractor,element,by } from 'protractor';
import { expect } from 'chai';

describe('When: I use the reading list feature', () => {
  
  it('Undo action on removing item', async () => {
  await browser.get('/');
  await browser.wait(
    ExpectedConditions.textToBePresentInElement($('tmo-root'), 'okreads')
  );

  const form = await $('form');
  const input = await $('input[type="search"]');
  await input.sendKeys('javascript');
  await form.submit();
  
  var until = protractor.ExpectedConditions;
  browser.wait(until.presenceOf(element.all(by.className('book--title')).first()), 180000, 'Element taking too long to appear in the DOM');
   
  let datatesting = await $('[data-testing="book-item"]');
  let bookcontent  = await datatesting.$('[data-testing="book-item"] > div.book--content');
  let bookinfo = await bookcontent.$('div.book--content > div.book--content--info');
  let bookinfolist =  await bookinfo.$('div.book--content--info > div > button.mat-primary');
 
  
  await bookinfolist.click();

  await $('[data-testing="toggle-reading-list"]').click();
 


  
  //$('[data-testing="book-item"] .book--content .book--content--info').lastElementChild.firstElementChild.click();
 
  //await $('[data-testing="reading-list-container"] .reading-list-content tmo-reading-list').lastElementChild.firstElementChild.click();

  /*const EC = protractor.ExpectedConditions;   
 const snackBar = element(by.tagName('simple-snack-bar'));
    browser.wait(EC.visibilityOf(snackBar), 30000);*/
   // $(".mat-simple-snackbar .mat-simple-snackbar-action").firstElementChild.click();
   // expect($('[data-testing="reading-list-container"] .reading-list-content')).equals("You haven't added any books to your reading list yet.");
    
});
});
