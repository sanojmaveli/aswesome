
import { Book } from '@tmo/shared/models';
import { ReadingListService } from './reading-list.service';
import { Body,Controller,Delete,Get,Param,Post,Put} from '@nestjs/common';

@Controller()
export class ReadingListController {
  constructor(private readonly readingList: ReadingListService) {}

  @Get('/reading-list/')
  async getReadingList() {
    return await this.readingList.getList();
  }

  @Post('/reading-list/')
  async addToReadingList(@Body() item: Book) {
    return await this.readingList.addBook(item);
  }

  @Delete('/reading-list/:id')
  async removeFromReadingList(@Param() params) {
    return await this.readingList.removeBook(params.id);
  }

  @Put('/reading-list/:id/finished')
    async markBookAsFinished(@Param() params){
        return await this.readingList.markFinished(params.id);
    }
}
